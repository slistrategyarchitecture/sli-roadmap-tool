package com.cloudnan.util;

public class InvalidExcelDocumentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidExcelDocumentException() {
		super();
	}

	public InvalidExcelDocumentException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public InvalidExcelDocumentException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public InvalidExcelDocumentException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
