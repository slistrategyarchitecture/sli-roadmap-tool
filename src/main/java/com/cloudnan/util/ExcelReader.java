package com.cloudnan.util;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.jboss.logging.Logger;

import com.cloudnan.RagStatus;
import com.sli.roadmap.Project;
import com.sli.roadmap.Technology;

public class ExcelReader {
	
	public static final int MY_MINIMUM_COLUMN_COUNT = 10;
	
	public static void processTechnologyExcel(File file) throws InvalidExcelDocumentException {

		try {
			Workbook wb = WorkbookFactory.create(file);
			Sheet sheet = wb.getSheetAt(0);
			for (int rowNumber = 1; rowNumber < sheet.getLastRowNum(); rowNumber++) {
				Row row = sheet.getRow(rowNumber);
				if (row != null) {
					processTechnologyRecord(row);
				}
		    }
		} catch (InvalidFormatException ife )  {
			// TODO Auto-generated catch block
			ife.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void processTechnologyRecord(Row row) {
		Technology technology = null;
    	Cell cell = null;
    	// get the project identifier first
    	Cell productCell = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
    	Cell versionCell = row.getCell(2, Row.RETURN_BLANK_AS_NULL);
    	
		// check o see if this technology already exists in the database
		if (productCell != null && versionCell != null && 
			versionCell.getCellType() == Cell.CELL_TYPE_STRING && 
			productCell.getCellType() == Cell.CELL_TYPE_STRING) {
				String productName = productCell.getStringCellValue();
				String version = versionCell.getStringCellValue();
				try {
					technology = Technology.findTechnologysByProductNameLikeAndVersionLike(productName, version).getSingleResult();
				} catch (org.springframework.dao.EmptyResultDataAccessException e) {
		    		technology = new Technology();
		    		technology.setProductName(productName);
		    		technology.setVersion(version);
		    	}
   		} else {
			Logger.getLogger(ExcelReader.class).info("Failed to identify version and product name for row " + row.getRowNum());
			return;
		}
    	
    	
    	//  Get the VENDOR NAME cell (first cell = 0)
    	cell = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		technology.setVendorName(cell.getStringCellValue());
	    }
    	
    	//  Get the TARGET PATCH LEVEL cell 
    	cell = row.getCell(3, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		technology.setTargetPatchLevel(cell.getStringCellValue());
	    }
    	
    	//  Get the TECHNICAL ARCHITECTURE
    	cell = row.getCell(4, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		technology.setTechnicalArchitecture(cell.getStringCellValue());
	    }
    	
    	//  Get the OWNER
    	cell = row.getCell(5, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		technology.setOwner(cell.getStringCellValue());
	    }
    	
    	// Get the General Availability 
    	cell = row.getCell(6, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
    		Date cellDate = cell.getDateCellValue();
    		Calendar date = GregorianCalendar.getInstance();
    		date.setTime(cellDate);
    		technology.setGeneralAvailability(date);
	    }
    	
    	// get the mainstream retirement
    	cell = row.getCell(7, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
    		Date cellDate = cell.getDateCellValue();
    		Calendar date = GregorianCalendar.getInstance();
    		date.setTime(cellDate);
    		technology.setMainstreamRetirement(date);
	    }
    	    	
    	// get the extended retirement
    	cell = row.getCell(8, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
    		Date cellDate = cell.getDateCellValue();
    		Calendar date = GregorianCalendar.getInstance();
    		date.setTime(cellDate);
    		technology.setExtendedRetirement(date);
	    }
    	
    	//  Get the PROJECT RAG STATUS
    	cell = row.getCell(9, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		String ragStatus = cell.getStringCellValue();
    		if (ragStatus.equalsIgnoreCase("R")) {
    			technology.setRagStatus(RagStatus.Red);
    		} else if (ragStatus.equalsIgnoreCase("A")) {
    			technology.setRagStatus(RagStatus.Amber);
    		} else if (ragStatus.equalsIgnoreCase("G")) {
    			technology.setRagStatus(RagStatus.Green);
    		} else {
    			technology.setRagStatus(RagStatus.Blue);
    			Logger.getLogger(ExcelReader.class).debug("Failed to indentify RAG status for row " + row.getRowNum() + " defaulting to blue for " + technology.getProductName());
    		}
	    } 
    	
    	// Get the NOTES
    	cell = row.getCell(10, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		technology.setNotes(cell.getStringCellValue());
	    }
    	
    	// Get the WEBSITE
    	cell = row.getCell(11, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		technology.setWebsiteURL(cell.getStringCellValue());
	    }
    	technology.persist();
	}


	
	public static void processProjectExcel(File file) throws InvalidExcelDocumentException {

		try {
			Workbook wb = WorkbookFactory.create(file);
			Sheet sheet = wb.getSheetAt(0);
			for (int rowNumber = 1; rowNumber < sheet.getLastRowNum(); rowNumber++) {
				Row row = sheet.getRow(rowNumber);
				if (row != null) {
					processProjectRecord(row);
				}
		    }
		} catch (InvalidFormatException ife )  {
			// TODO Auto-generated catch block
			ife.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void processProjectRecord(Row row) {
		Project project = null;
    	Cell cell = null;
    	// get the project identifier first
    	cell = row.getCell(2, Row.RETURN_BLANK_AS_NULL);
    	
    	try {
    		if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    				String projectIdentifier = cell.getStringCellValue();
    				project = Project.findProjectsByProjectNameEquals(projectIdentifier).getSingleResult();
    		} else {
    			Logger.getLogger(ExcelReader.class).info("Failed to identify version and product name for row " + row.getRowNum());
    			return;
    		}
    	} catch (org.springframework.dao.EmptyResultDataAccessException e) {
    		project = new Project();
    	}
    	
    	//  Get the PORTFOLIO cell (first cell = 0)
    	cell = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		project.setPortfolio(cell.getStringCellValue());
	    }
    	
    	//  Get the PROGRAMME cell 
    	cell = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		project.setProgramme(cell.getStringCellValue());
	    }
    	
    	//  Get the PROJECT IDENTIFIER
    	cell = row.getCell(2, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		project.setProjectIdentifier(cell.getStringCellValue());
	    }
    	
    	//  Get the PROJECT NAME
    	cell = row.getCell(3, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		project.setProjectName(cell.getStringCellValue());
	    }
    	
    	//  Get the PROJECT RAG STATUS
    	cell = row.getCell(4, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		String ragStatus = cell.getStringCellValue();
    		if (ragStatus.equalsIgnoreCase("RED")) {
    			project.setStatus(RagStatus.Red);
    		} else if (ragStatus.equalsIgnoreCase("AMBER")) {
    			project.setStatus(RagStatus.Amber);
    		} else if (ragStatus.equalsIgnoreCase("GREEN")) {
    			project.setStatus(RagStatus.Green);
    		} else {
    			project.setStatus(RagStatus.Blue);
    		}
	    } 
    	
    	// Get the PROJECT SIGNIFICANCE
    	cell = row.getCell(5, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
    		// do nothing
	    }
    	
    	// Get the SHORT DESC
    	cell = row.getCell(6, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		project.setShortDescription(cell.getStringCellValue());
	    }
    	
    	//  Get the PROJECT GOAL
    	cell = row.getCell(7, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		project.setProjectGoal(cell.getStringCellValue());
	    }
    	
    	//  Get the PROJECT STAGE
    	cell = row.getCell(8, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
    		project.setProjectStage(cell.getStringCellValue());
	    }
    	
    	// ignore RAG image (number 9)
    	
    	//  Get the PROJECT START DATE
    	cell = row.getCell(10, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
    		Date cellDate = cell.getDateCellValue();
    		Calendar startDate = GregorianCalendar.getInstance();
    		startDate.setTime(cellDate);
    		project.setStartDate(startDate);
	    }
    	
    	//  Get the PROJECT COMPLETION DATE
    	cell = row.getCell(11, Row.RETURN_BLANK_AS_NULL);
    	if (cell != null && cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
    		Calendar completionDate = GregorianCalendar.getInstance();
    		Date compDateVal = cell.getDateCellValue(); 
    		completionDate.setTime(compDateVal);
    		project.setCompletionDate(completionDate);
	    }
    	project.persist();
	}
}
