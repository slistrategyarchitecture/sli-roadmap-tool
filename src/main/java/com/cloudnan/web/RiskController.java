package com.cloudnan.web;
import com.sli.roadmap.Risk;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/risks")
@Controller
@RooWebScaffold(path = "risks", formBackingObject = Risk.class)
public class RiskController {
}
