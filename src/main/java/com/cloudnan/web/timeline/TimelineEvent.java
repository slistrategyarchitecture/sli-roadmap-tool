package com.cloudnan.web.timeline;

import java.util.Date;

import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
public class TimelineEvent {

	private String eventHeading;
	private int eventType;
	private Date eventDate;
	private String eventDescription;
	
	public TimelineEvent() {
		// TODO Auto-generated constructor stub
	}

}
