package com.cloudnan.web.timeline;

import java.util.ArrayList;
import java.util.List;

import org.springframework.roo.addon.javabean.RooJavaBean;

import com.sli.roadmap.Application;

@RooJavaBean
public class Timeline {

	private List<Application> applications;
	private String applicationName;
	private List<TimelineEvent> events;
	
	public Timeline() {
		applications = new ArrayList<Application>();
		events = new ArrayList<TimelineEvent>();
	}

}
