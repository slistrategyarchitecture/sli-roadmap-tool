package com.cloudnan.web;
import com.sli.roadmap.Technology;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/technologys")
@Controller
@RooWebScaffold(path = "technologys", formBackingObject = Technology.class)
public class TechnologyController {
}
