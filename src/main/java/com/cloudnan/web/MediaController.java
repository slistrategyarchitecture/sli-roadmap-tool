package com.cloudnan.web;
import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.cloudnan.util.ExcelReader;
import com.sli.roadmap.MediaUpload;

@RequestMapping("/upload")
@Controller
@RooWebScaffold(path = "upload", formBackingObject = MediaUpload.class)
public class MediaController {
	
	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
		uiModel.addAttribute("mediaUpload", new MediaUpload());
        return "upload/create";
    }
	
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid MediaUpload mediaUpload, BindingResult bindingResult, Model uiModel,
    	       @RequestParam("content") CommonsMultipartFile content,
    	       HttpServletRequest httpServletRequest) {
    	String originalFilename = content.getOriginalFilename();
    	File dest = new File("/tmp/" + originalFilename);
    	try {
    		content.transferTo(dest);
    		if (mediaUpload.getContentType().equalsIgnoreCase("Project")) {
    	    	ExcelReader.processProjectExcel(dest);
    	    } else {
    	    	ExcelReader.processTechnologyExcel(dest);
    	    }
    	    mediaUpload.setFilesize(content.getSize());
    	    mediaUpload.setFilePath(dest.getAbsolutePath());
    	    mediaUpload.setContentType(content.getContentType());
    	    mediaUpload.persist();
    	    
    	} catch (Exception e) {
    		e.printStackTrace();
    	    return "upload/create";
    	}
        uiModel.asMap().clear();
        return "redirect:/upload/" + encodeUrlPathSegment(mediaUpload.getId().toString(), httpServletRequest);
    }
    
   
}
