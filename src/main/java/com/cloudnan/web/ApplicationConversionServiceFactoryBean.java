package com.cloudnan.web;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.roo.addon.web.mvc.controller.converter.RooConversionService;

import com.sli.roadmap.Application;
import com.sli.roadmap.Project;
import com.sli.roadmap.Technology;

/**
 * A central place to register application converters and formatters. 
 */
@RooConversionService
public class ApplicationConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {

	public Converter<Application, String> getApplicationToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.sli.roadmap.Application, java.lang.String>() {
            public String convert(Application application) {
                return new StringBuilder().append(application.getApplicationName()).toString();
            }
        };
    }
	
	 public Converter<Technology, String> getTechnologyToStringConverter() {
	        return new org.springframework.core.convert.converter.Converter<com.sli.roadmap.Technology, java.lang.String>() {
	            public String convert(Technology technology) {
	                return new StringBuilder().append(technology.getVendorName()).append(" - ").append(technology.getProductName()).append(' ').append(technology.getVersion()).append(' ').toString();
	            }
	        };
	    }
	    
	 
	 public Converter<Project, String> getProjectToStringConverter() {
	        return new org.springframework.core.convert.converter.Converter<com.sli.roadmap.Project, java.lang.String>() {
	            public String convert(Project project) {
	                return new StringBuilder().append(project.getProjectIdentifier()).append(" - ").append(project.getProjectName()).toString();
	            }
	        };
	    } 
	
	@Override
	protected void installFormatters(FormatterRegistry registry) {
		super.installFormatters(registry);
		// Register application converters and formatters
	}
}
