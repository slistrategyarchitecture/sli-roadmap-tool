package com.cloudnan.web;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cloudnan.web.timeline.Timeline;
import com.cloudnan.web.timeline.TimelineEvent;
import com.sli.roadmap.Application;
import com.sli.roadmap.Event;
import com.sli.roadmap.Project;
import com.sli.roadmap.Risk;
import com.sli.roadmap.Technology;

@RequestMapping("/roadmap/**")
@Controller
public class RoadmapController {

    @RequestMapping(method = RequestMethod.POST, value = "{id}")
    public void post(@PathVariable Long id, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(@ModelAttribute("applicationName") String applicationName, Model uiModel) {
    	Timeline timeline = new Timeline();
    	timeline.setApplications(Application.findAllApplications());
    	timeline.setApplicationName("");
    	uiModel.addAttribute("timeline", timeline);
        return "roadmap/index";
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public String getRoadmap(@ModelAttribute("applicationName") String applicationName, Model uiModel) {
    	// retrieve a list of all events , risk, technologies order where date > today
    	Timeline timeline = new Timeline();
    	timeline.setApplications(Application.findAllApplications());
    	timeline.setApplicationName(applicationName);
    	try {
    		Application application = Application.findApplicationsByApplicationNameLike(applicationName).getSingleResult();
    		// get the projects which involve this application and add a timeline event for the start date and completion date if not null to the response
    		List<Project> projects = application.getProjects();
    		addProjectsToTimeline(timeline, projects);
    		List<Technology> technologies = application.getTechnologies();
    		addTechnologyEventsToTimeline(timeline, technologies);
    		
    		// get the technologies used by the application and create an timeline event for the eol events.
    	} catch (Exception e) {
    		Logger.getLogger(RoadmapController.class).error("Failed to build application timeline for " + applicationName);
    		e.printStackTrace();
    	}
    	uiModel.addAttribute(timeline);
        return "roadmap/index";
    }
    
    private void addTechnologyEventsToTimeline(Timeline timeline,
			List<Technology> technologies) {
    	Iterator<Technology> technologyIterator = technologies.listIterator();
		while(technologyIterator.hasNext()) {
			Technology technology = technologyIterator.next();
			// build release date event
			if (technology.getGeneralAvailability() != null) {
				TimelineEvent event = new TimelineEvent();
				event.setEventType(2);
				event.setEventDescription(technology.getProductName() + " " +
						technology.getVersion() + " was released ");
				event.setEventHeading(technology.getProductName() + " " +
						technology.getVersion());
				event.setEventDate(technology.getGeneralAvailability().getTime());
				timeline.getEvents().add(event);
			}
			// build mainstream end of support date event
			if (technology.getMainstreamRetirement() != null) {
				TimelineEvent event = new TimelineEvent();
				event.setEventType(2);
				event.setEventHeading(technology.getProductName() + " " +
						technology.getVersion());
				event.setEventDescription(technology.getProductName() + " " +
						technology.getVersion() + " mainstream support ends.");
				event.setEventDate(technology.getMainstreamRetirement().getTime());
				timeline.getEvents().add(event);
			}
		}		
	}

	private void addProjectsToTimeline(Timeline timeline, List<Project> projects) {
    	Iterator<Project> projectIterator = projects.listIterator();
		while(projectIterator.hasNext()) {
			Project project = projectIterator.next();
			if (project.getStartDate() != null) {
				TimelineEvent event = new TimelineEvent();
				event.setEventType(1);
				event.setEventDescription(project.getShortDescription());
				event.setEventHeading(project.getProjectName());
				event.setEventDate(project.getStartDate().getTime());
				timeline.getEvents().add(event);
			}
		}
    }
	
	private void addRisksToTimeline(Timeline timeline, List<Risk> risks) {
    	Iterator<Risk> riskIterator = risks.listIterator();
		while(riskIterator.hasNext()) {
			Risk risk = riskIterator.next();
			if (risk.getEffectiveDate() != null) {
				TimelineEvent event = new TimelineEvent();
				event.setEventType(3);
				event.setEventDescription(risk.getDescription());
				event.setEventHeading(risk.getRiskName());
				event.setEventDate(risk.getEffectiveDate().getTime());
				timeline.getEvents().add(event);
			}
		}
    }
	
	private void addEventsToTimeline(Timeline timeline, List<Event> events) {
    	Iterator<Event> eventIterator = events.listIterator();
		while(eventIterator.hasNext()) {
			Event applicationEvent = eventIterator.next();
			if (applicationEvent.getEventDate() != null) {
				TimelineEvent event = new TimelineEvent();
				event.setEventType(4);
				event.setEventDescription(applicationEvent.getDescription());
				event.setEventHeading(applicationEvent.getName());
				event.setEventDate(applicationEvent.getEventDate().getTime());
				timeline.getEvents().add(event);
			}
		}
    }
}
