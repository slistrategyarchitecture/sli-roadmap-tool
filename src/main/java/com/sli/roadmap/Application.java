package com.sli.roadmap;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Size;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findApplicationsByApplicationNameLike", "findApplicationsByApplicationNameEquals" })
public class Application {

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Project> projects = new ArrayList<Project>();

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Technology> technologies = new ArrayList<Technology>();

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Risk> risks = new ArrayList<Risk>();

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Event> events = new ArrayList<Event>();

    /**
     */
    private String applicationName;
    
    @Size(max = 4096)
    private String applicationDescription;
}
