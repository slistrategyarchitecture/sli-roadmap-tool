package com.sli.roadmap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class MediaUpload {

	public static List<String> fileTypes;
	static{
	  ArrayList<String> tmp = new ArrayList<String>();
	  tmp.add("Technology");
	  tmp.add("Project");
	  fileTypes = Collections.unmodifiableList(tmp);
	}
	
	public List<String> getfileTypes() {
	    return fileTypes;
	}
	
	@Transient
	private byte[] content;
	
	/**
     */
    @NotNull
    @Size(max = 128)
    private String filePath;

    /**
     */
    @NotNull
    private long filesize;

    /**
     */
    @NotNull
    private String contentType;
    
    public MediaUpload() {
    }
}
