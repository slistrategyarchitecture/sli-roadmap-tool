package com.sli.roadmap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cloudnan.RagStatus;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findProjectsByProjectIdentifierLike", "findProjectsByProjectNameEquals" })
public class Project {

    /**
     */
    private String portfolio;

    /**
     */
    private String programme;

    /**
     */
    private String projectIdentifier;

    /**
     */
    private String projectName;

    /**
     */
    @Enumerated
    private RagStatus status;

    /**
     */
    @Size(max = 2048)
    private String shortDescription;

    /**
     */
    private String projectGoal;

    /**
     */
    private String projectStage;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar startDate;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar completionDate;

    /**
     */
    @NumberFormat(style = Style.CURRENCY)
    private double totalPlannedCost;

    /**
     */
    private String totalPlannedCostCurrency;

    /**
     */
    @NumberFormat(style = Style.CURRENCY)
    private double totalActualCost;

    /**
     */
    private String totalActualCostCurrency;

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Application> applications = new ArrayList<Application>();
}
