package com.sli.roadmap;
import java.util.Calendar;

import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cloudnan.RagStatus;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findTechnologysByProductNameLikeAndVersionLike" })
public class Technology {

    /**
     */
    private String vendorName;

    /**
     */
    private String productName;

    /**
     */
    private String version;

    /**
     */
    private String targetPatchLevel;

    /**
     */
    private String technicalArchitecture;

    /**
     */
    private String owner;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar generalAvailability;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar mainstreamRetirement;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar extendedRetirement;

    /**
     */
    @Size(max = 4096)
    private String notes;

    /**
     */
    private String websiteURL;

    /**
     */
    @Enumerated
    private RagStatus ragStatus;
}
