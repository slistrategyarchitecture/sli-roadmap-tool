// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sli.roadmap;

import com.sli.roadmap.Risk;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect Risk_Roo_Configurable {
    
    declare @type: Risk: @Configurable;
    
}
